package controller

import (
	"net/http"
	"strings"

	"github.com/labstack/echo"
	"gitlab.com/ferhatcandas/technologist/api/request"
	"gitlab.com/ferhatcandas/technologist/api/service"
)

type TodoController struct {
	service service.TodoService
}

func TodoControllerInstance(service service.TodoService) *TodoController {
	return &TodoController{
		service: service,
	}
}
func (c *TodoController) GetTodos(ctx echo.Context) (err error) {

	list := c.service.GetTodos()

	return ctx.JSON(http.StatusOK, list)
}
func (c *TodoController) Ok(ctx echo.Context) (err error) {

	return ctx.JSON(http.StatusOK, "")
}

func (c *TodoController) NewTodo(ctx echo.Context) (err error) {

	req := new(request.TodoRequest)
	if err = ctx.Bind(req); err != nil {
		return ctx.JSON(http.StatusBadRequest, "Error while binding request")
	}
	if req.Text == "" || len(strings.Trim(req.Text, " ")) == 0 {
		return ctx.JSON(http.StatusBadRequest, "Text must greather than 0 character")
	}
	c.service.AddNewTodo(req.Text)

	return ctx.JSON(http.StatusOK, nil)
}
