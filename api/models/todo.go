package models

import (
	uuid "github.com/satori/go.uuid"
)

type TodoEntity struct {
	Id   string `json:"id"`
	Text string `json:"text"`
}

func NewTodo(newText string) TodoEntity {
	return TodoEntity{
		Text: newText,
		Id:   uuid.NewV4().String(),
	}
}
