package models_test

import (
	"testing"

	"gitlab.com/ferhatcandas/technologist/api/models"
)

func CreateTodoEntity(t *testing.T) {
	todo := models.NewTodo("buy some milk")
	if todo.Text != "buy some milk" && todo.Id != "" {
		t.Error("Assertion is not equal")
	}
}
