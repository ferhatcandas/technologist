package data

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/go-redis/redis/v8"
	"gitlab.com/ferhatcandas/technologist/api/models"
)

var (
	ctx = context.Background()
)

type RedisCache struct {
	db *redis.Client
}

func RedisInstance(addr string, pass string) *RedisCache {
	return &RedisCache{
		db: redis.NewClient(&redis.Options{
			Addr:     addr,
			Password: pass, // no password set
			DB:       0,    // use default DB
		}),
	}
}

func (r *RedisCache) GetAll() (todos []models.TodoEntity) {
	response := []models.TodoEntity{}
	if members, errs := r.db.SMembers(ctx, "todos").Result(); errs != nil {
		panic(errs)
	} else {
		for i := 0; i < len(members); i++ {
			item := []byte(members[i])
			var newData models.TodoEntity
			if err := json.Unmarshal(item, &newData); err != nil {
				panic(err)
			}
			response = append(response, newData)
		}
		return response
	}

}

func (r *RedisCache) Add(todo models.TodoEntity) {
	b, err := json.Marshal(todo)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(b))

	if err := r.db.SAdd(ctx, "todos", b).Err(); err != nil {
		panic(err)
	}
}
