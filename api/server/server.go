package server

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gitlab.com/ferhatcandas/technologist/api/controller"
	"gitlab.com/ferhatcandas/technologist/api/data"
	"gitlab.com/ferhatcandas/technologist/api/service"
)

type TodoAPI struct {
	e *echo.Echo
}

func Instance() *TodoAPI {
	return &TodoAPI{
		e: echo.New(),
	}
}

// Start server functionality
func (s *TodoAPI) Start(port string) {

	var redisClient = data.RedisInstance("localhost:6379", "")
	var todoService = service.TodoInstance(*redisClient)
	var todoController = controller.TodoControllerInstance(*todoService)

	s.e.Use(middleware.Logger())

	s.e.Use(middleware.Recover())

	s.e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE},
	}))

	s.e.GET("/hc", todoController.Ok)
	s.e.GET("/todos", todoController.GetTodos)
	s.e.POST("/todos", todoController.NewTodo)
	s.e.Logger.Fatal(s.e.Start(port))
}

func (s *TodoAPI) Close() {
	s.e.Close()
}
