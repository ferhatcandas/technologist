package service_test

import (
	"testing"

	"gitlab.com/ferhatcandas/technologist/api/data"
	"gitlab.com/ferhatcandas/technologist/api/service"
)

var todoService = service.TodoInstance(*data.RedisInstance("localhost:6379", ""))

func AddNewTodoTest(t *testing.T) {
	err := todoService.AddNewTodo("test")
	if err != nil {
		t.Error("Expected error is nil")
	}
}
func GetTodoTest(t *testing.T) {
	err := todoService.GetTodos()
	if err == nil {
		t.Error("Expected error is not nil")
	}
}
