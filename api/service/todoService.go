package service

import (
	"gitlab.com/ferhatcandas/technologist/api/data"
	"gitlab.com/ferhatcandas/technologist/api/models"
)

type TodoService struct {
	RedisClient data.RedisCache
}

func TodoInstance(redis data.RedisCache) *TodoService {
	return &TodoService{
		RedisClient: redis,
	}
}

func (s *TodoService) AddNewTodo(text string) error {

	var newTodo models.TodoEntity = models.NewTodo(text)
	s.RedisClient.Add(newTodo)
	return nil
}
func (s *TodoService) GetTodos() []models.TodoEntity {

	return s.RedisClient.GetAll()
}
