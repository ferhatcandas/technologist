package main

import (
	server "gitlab.com/ferhatcandas/technologist/api/server"
)

func main() {

	s := server.Instance()
	s.Start(":8000")
}
