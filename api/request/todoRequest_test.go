package request_test

import (
	"testing"

	"gitlab.com/ferhatcandas/technologist/api/request"
)

func CreateTodoRequest(t *testing.T) {
	req := request.TodoRequest{Text: "buy some milk"}
	if req.Text != "buy some milk" {
		t.Error("Assertion is not equal")
	}
}
