import React from 'react'

export default function List(props) {
    const { list } = props
    return (
        <div>
            {
                list.map((element, index) =>
                    (<p id={index} >{element.text}</p>)
                )
            }
        </div>
    )
}