import React from 'react'
import Form from './form';
import List from './list';
import { todoService } from "../../services/todoService"

export default function TodoApp() {
    const [todos, setTodos] = React.useState([]);

    const getData = () => {
        todoService.get((data) => {
            setTodos(data)
        })
    }
    const postData = (value) => {
        todoService.post(value, () => {
            getData()
        })
    }
    React.useEffect(() => getData(), [])
    return (
        <div>
            {
                <>
                    <Form post={x => postData(x)}></Form>
                    <List list={todos}></List>
                </>
            }
        </div>
    )
}
