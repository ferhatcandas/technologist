import React from 'react'

export default function Form(props) {
    const { post } = props
    const [text, setText] = React.useState("");

    return (
        <div>
            <input name="todoTextBox" placeholder="write here.." value={text} onChange={(x) => setText(x.target.value)}></input><button role="addTodo" onClick={() => post(text)}>Add</button>
        </div>
    )
}
