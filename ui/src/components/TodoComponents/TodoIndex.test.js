import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import TodoApp from '.';

describe('Given Empty ToDo list', () => {
    test('When I write buy some milk to and click to Then I should see item in ToDo list', async () => {
        render(<TodoApp />);
        const button = await screen.findByRole("addTodo")
        const input = await screen.findByPlaceholderText("write here..")

        fireEvent.change(input, {
            target: { value: 'buy some milk' },
        })

        button.click()
        expect(screen.getByText('buy some milk')).toBeInTheDocument()
    });
})
