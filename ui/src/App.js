import logo from './logo.svg';
import './App.css';
import Todo from "./components/TodoComponents/index"

function App() {
  return (
    <div className="App">
      {<Todo></Todo>}
    </div>
  );
}

export default App;
