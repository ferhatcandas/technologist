import axios from 'axios';
const baseEndpoint = "http://139.59.152.172:30001/todos";

export const todoService = ({
    post: (text, callback) => {
        axios.post(baseEndpoint, { text: text }).then(res => {
            callback();
        })
    },
    get: (callback) => {
        axios.get(baseEndpoint).then((resp) => {
            callback(resp.data)
        })
    }
})