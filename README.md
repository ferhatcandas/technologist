### Todo App

There are 2 different applications in the project, backend service (golang) and frontend service (reactjs), in the simplest sense, only todo can be added, the application works on kubernetes, kubernetes is installed on digitalocean as a single node, test and production environments are separate, ci / cd processes are active and running, applications have their own pipelines, data is permanently kept in redis running in the same container as the backend service. 


FrontEnd : http://139.59.152.172:31002

BackEnd  : http://139.59.152.172:30001/todos

![s](docs/kubernetes.png)


